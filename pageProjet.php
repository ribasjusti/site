<!doctype html>
<html lang="fr">
  <head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-172843379-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-172843379-1');
    </script>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/style.css">

    <title>Justine Ribas | Projets</title>

    <link rel="icon" type="image/png" href="./images/photo_cv.png">

  </head>

  <body>
    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #96770e;">
      <div class="container-fluid">
        <a class="navbar-brand" href="index.html">Justine RIBAS</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="cv.html">CV</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="projets.php">Projets</a>
            </li>
            <!--
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="contact.php">Me contacter</a>
            </li>
           -->
          </ul>
        </div>
      </div>
    </nav>

    <?php
      if(empty($_GET)){
        header('Location: projets.php');
      }

      /*Fonction pour récupérer les informations du projet*/
      function recupProjet(){
        $row = 0;
        $projet = array(); //tableau qui contient les info du projet
        $tabKeys = array(); //tableau qui contient toutes les clés du tableau $projet
        if (($handle = fopen("./projets.csv", "r"))) {
          while (($data = fgetcsv($handle, 1000, ";"))) {
            if($row == 0){
              // si on est à la première ligne du csv, on récupère les clés
              $tabKeys = $data;
            } else {
              if($data[0] == $_GET["projet"]){
                $i = 0;
                // on construit notre tableau avec la ligne correspondant au projet et les clés
                foreach ($tabKeys as $key) {
                  $projet[$key] = $data[$i];
                  $i ++;
                }
              }
            }
            $row++;
          }
          fclose($handle);
        }
        return($projet);
      }

      $projet = recupProjet();
    ?>

    <div style="text-align:center">
      <?php
      echo "<h1 class='my-2'>".$projet["titre"]."</h1>
      <div class='container'>
        <div class='row my-5'>
          <div class='col-lg-4 col-md-6'>
            <img src='images/".$projet["id"].".png' style='width:100%;'/>
          </div>
          <div style='text-align:center;' class='col-lg-8 col-md-6'>
            <h6>Date de début : ".$projet["dateDeb"]."</h6>
            <h6>Date de fin : ".$projet["dateFin"]."</h6>
            <p class='mx-3' style='text-align:left; font-size:1.2em'>".$projet["description"]."</p>
          </div>
        </div>";
      if($projet["gitLab"] != ""){
        echo("<a target='_blank' href='".$projet["gitLab"]."'><h6>Code source sur Git Lab</h6></a>
        </div>");
      } else {
        echo("<a target='_blank' href='".$projet["url"]."'><h6>Url du site</h6>
        </div>");
      }
      ?>
    </div>

  </body>
</html>
