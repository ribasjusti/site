<!doctype html>
<html lang="fr">
  <head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-172843379-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-172843379-1');
    </script>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/style.css">

    <title>Justine Ribas | Contact</title>

    <link rel="icon" type="image/png" href="./images/photo_cv.png">

  </head>

  <body>
    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #96770e;">
      <div class="container-fluid">
        <a class="navbar-brand" href="index.html">Justine RIBAS</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="cv.html">CV</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="projets.php">Projets</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="contact.php">Me contacter</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container">
      <form method="POST" action="#">
        <div class="mb-3">
          <label class="form-label">Votre adresse mail</label>
          <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
        </div>
        <div class="mb-3">
          <label class="form-label">Objet</label>
          <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
        </div>
        <div class="mb-3">
          <label class="form-label">Contenu</label>
          <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
      <p>Numéro de téléphone : +33 6 16 38 63 84</p>
    </div>

  </body>
</html>
