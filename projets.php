<!doctype html>
<html lang="fr">
  <head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-172843379-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-172843379-1');
    </script>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/style.css">

    <title>Justine Ribas | Projets</title>

    <link rel="icon" type="image/png" href="./images/photo_cv.png">

  </head>

  <body>
    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #96770e;">
      <div class="container-fluid">
        <a class="navbar-brand" href="index.html">Justine RIBAS</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="cv.html">CV</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="projets.php">Projets</a>
            </li>
            <!--
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="contact.php">Me contacter</a>
            </li>
           -->
          </ul>
        </div>
      </div>
    </nav>

    <div class="container">

      <div style="text-align:center;margin-bottom:30px;" class="mt-2">
        <?php

        if(empty($_GET) || $_GET["mot"] == ""){
          echo("<h1>Mes projets</h1>");
        } else {
          echo("<h1>Mes projets - ".$_GET["mot"]."</h1>");
        }

        ?>

      </div>

      <?php
      /*Fonction pour récupérer les informations de tous les projets*/
      function recupTabProjets(){
        $row = 0;
        $tabProjets = array(); //tableau qui contient les info de tous les projets
        $tabKeys = array(); //tableau qui contient toutes les clés du tableau $tabProjets
        if (($handle = fopen("./projets.csv", "r"))) {
          while (($data = fgetcsv($handle, 1000, ";"))) {
            if($row == 0){
              // si on est à la première ligne du csv, on récupère les clés
              $tabKeys = $data;
            } else {
              $recherche_ok = 1;
              $i = 0;
              // si une recherche a lieu, on vérifie que le projet corresponde à la recherche
              if(!empty($_GET) && $_GET["mot"] != ""){
                $recherche_ok = 0;
                $tabMots = explode("-", $data[2]);
                foreach ($tabMots as $mot) {
                  if($mot == $_GET["mot"]){
                    $recherche_ok = 1;
                  }
                }
              }
              // on construit notre tableau avec la ligne actuelle et les clés
              if($recherche_ok == 1){
                foreach ($tabKeys as $key) {
                  $tabProjets[$row-1][$key] = $data[$i];
                  $i ++;
                }
              }
            }
            $row++;
          }
          fclose($handle);
        }
        return($tabProjets);
      }

      /*Fonction pour récupérer les catégories*/
      function recupCategories(){
        $row = 0;
        $tabCategories = array(); //tableau qui contient les catégories
        if (($handle = fopen("./projets.csv", "r"))) {
          while (($data = fgetcsv($handle, 1000, ";"))) {
            $mots = explode("-", $data[2]);
            foreach ($mots as $mot) {
              if(!in_array($mot, $tabCategories)){
                array_push($tabCategories, $mot);
              }
            }
            $row++;
          }
          fclose($handle);
        }
        return($tabCategories);
      }

      $tabProjets = recupTabProjets();

      $tabCategories = recupCategories();
      ?>

      <div class="container">
        <form method="GET" action="projets.php">
          <select class="mb-4" name="mot">
              <option value="">Catégorie</option>
              <?php
                foreach ($tabCategories as $value) {
                  echo("<option value='".$value."'>".$value."</option>");
                }
              ?>
          </select>
          <button type="submit" class="btn btn-outline-dark" style="padding:10px;">Rechercher</button>
        </form>
      </div>



      <div class="row">
        <?php

        $i = 0;
        foreach ($tabProjets as $projet) {
          $i ++;
          $tabMots = explode("-", $projet["mots"]);
          echo("<div class='col-lg-4 col-md-6'>
          <div class='projet'>
            <div id='".$projet["id"]."' onclick='allerPageProjet(this)'>
              <h4>".$projet["titre"]."</h4>
              <img src='images/".$projet["id"].".png' style='width:50%'/>
              <h6 style='margin-top:10px'>Mots-clés :</h6>
            </div>
            <div class='mots'>");
          $j = 0;
          foreach ($tabMots as $mot) {
            $j ++;
            echo("<div class='mot' id='".$mot."' onclick='rechercher(this)'>".$mot."</div>");
            if($j % 3 == 0){
              echo('</div><div class="mots">');
            }
          }
          echo("</div></div></div>");
          if($i % 3 == 0){
            echo('</div><div class="row">');
          }
        }

        ?>
      </div>
    </div>



  <script>
    function allerPageProjet(projet){
      document.location.href = "pageProjet.php?projet=" + projet.id;
    }

    function rechercher(mot){
      document.location.href = "projets.php?mot=" + mot.id;
    }
  </script>

  </body>
</html>
